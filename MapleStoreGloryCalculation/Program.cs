﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MapleStoreGloryCalculation
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("請輸入要衝的榮耀卷類型 (1)裝備(2)寵物");
            var category =  Console.ReadLine();
            if (category == "1")
            {
                PetGloryCalculation();
            }
            else
            {
                EquipmentGloryCalculation();
            }
        }
        
        //榮耀裝備計算
        private static void EquipmentGloryCalculation()
        {
            try
            {
                Console.WriteLine("請輸入衝卷數");
                int count = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("請輸入主屬數值(含星火)");
                int basicMainAttribute = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("請輸入攻擊數值(含星火)");
                int basicAtk = Convert.ToInt32(Console.ReadLine());
            
                var mainattributeSum = basicMainAttribute;
                var atkSum = basicAtk;
                List<int> mainAttribute = new List<int>();
                List<int> weaponatk = new List<int>();

                for (var i = 1; i <= count; i++)
                {
                    Console.WriteLine($"請輸入第{i}次衝完卷後的主要屬性及ATK");
                    var mainattribute = Convert.ToInt32(Console.ReadLine()); //Getting an input from the user.
                    var atk = Convert.ToInt32(Console.ReadLine());
    
                    int magnetiteAvenge;
                    int atkAvenge;
                    
                    if (i == 1)
                    {
                        Console.WriteLine($"第{i}卷，目前總主屬為{mainattribute}，此次衝卷榮耀主屬為{mainattribute - basicMainAttribute}"); //Print the input.
                        Console.WriteLine($"第{i}卷，目前總攻擊為{atk}，此次衝卷榮耀攻擊為{atk - basicAtk}");
        
                        magnetiteAvenge = mainattribute - basicMainAttribute;
                        atkAvenge = atk - basicAtk;
                        
                        mainattributeSum = mainattribute;
                        atkSum = atk;
                        
                        mainAttribute.Add(magnetiteAvenge);
                        weaponatk.Add(atkAvenge);
                    }
                    else
                    {
                        Console.WriteLine($"第{i}卷，目前總主屬為{mainattribute}，此次衝卷榮耀主屬為{mainattribute - mainattributeSum}"); //Print the input.
                        Console.WriteLine($"第{i}卷，目前總攻擊為{atk}，此次衝卷榮耀攻擊為{atk - atkSum}");
                        
                        magnetiteAvenge = mainattribute - mainattributeSum;
                        atkAvenge = atk - atkSum;
                        
                        mainattributeSum = mainattribute;
                        atkSum = atk;
                        
                        mainAttribute.Add(magnetiteAvenge);
                        weaponatk.Add(atkAvenge); 
                    }
                        
                }
                
                var totalMainAttribute = mainAttribute.Sum(x => Convert.ToDouble((x)));
                var totalAtk = weaponatk.Sum(x => Convert.ToDouble((x)));
                Console.WriteLine("此次榮耀衝完的結果如下");
                Console.WriteLine($"此次衝卷數為{count}");
                Console.WriteLine($"此次衝完平均主屬為{totalMainAttribute / count}");
                Console.WriteLine($"此次衝完平均(物/魔)ATK為{totalAtk / count}");
                Console.WriteLine("按任意鍵結束....");
                Console.ReadKey();  //可按任意鍵結束畫面
            }
            catch (Exception e)
            {
                Console.WriteLine("壞掉了啦，麻煩輸入數字好嗎");
                Console.WriteLine(e);
                throw;
            }
        }
        
        //寵物榮耀裝備計算
        private static void PetGloryCalculation()
        {
            try
            {
                const int count = 10;
                int petAtkSum = 0; //寵物裝備初始皆為0
                int lastAtk = 0;
                
                List<int> petAtk = new List<int>();

                for (var i = 1; i <= count; i++)
                {
                    Console.WriteLine("寵物裝備固定10卷，請直接輸入衝完數字即可");
                    var atk = Convert.ToInt32(Console.ReadLine());
                    
                    int petAtkAvenge;
                    
                    if (i == 1)
                    {
                        Console.WriteLine($"第{i}卷，目前總攻擊為{atk}，此次衝卷榮耀攻擊為{atk}");
                        
                        petAtkAvenge = atk;
                        petAtkSum = atk;
                        lastAtk = atk;
                        petAtk.Add(petAtkAvenge);
                    }
                    else
                    {
                        Console.WriteLine($"第{i}卷，目前總攻擊為{atk}，此次衝卷榮耀攻擊為{atk - petAtkSum}");
                        
                        petAtkAvenge = atk - petAtkSum;
                        petAtkSum = atk;
                        petAtk.Add(petAtkAvenge);
                    }
                }

                var totalPetAtk = petAtk.Sum(x => Convert.ToDouble((x)));
                Console.WriteLine("此次榮耀衝完的結果如下");
                Console.WriteLine($"此次衝完平均(物/魔)ATK為{totalPetAtk / 10}");
                Console.WriteLine("按任意鍵結束....");
                Console.ReadKey();  //可按任意鍵結束畫面
            }
            catch (Exception e)
            {
                Console.WriteLine("壞掉了啦，麻煩輸入數字好嗎");
                Console.WriteLine(e);
                throw;
            }
        }
    }
}